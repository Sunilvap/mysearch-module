Here is a module which provides search of nodes.
To install it you should copy it to drupal module's directory and enable it in admin stuff.

A direct url, such as http://domain.com/mysearch/example will search all nodes in a database for
occurrences of the word “example”.

---------------------------------------------------------------------

I changed mysearch_menu function by adding here
1. 'page arguments' => array(1) (it allows get second url argument with parameters in
menu callback function. For example http://site.dev/node/1 node is first (0) and 1 is second (1))
2. 'file' => 'mysearch.pages.inc' (I moved menu callback function to separated file. It is more comfortable)

Menu callback function mysearch_searchpage I wrote from scratch.
It has flexible code.
It allows us to search nodes not only by title or standart body field.
It looks on all text fields which are attached to content types.
In any time we can create some pagers, filters to search nodes.
