<?php

function mysearch_searchpage($str = ''){
  global $base_url;

  if($str){
    // if user search something tell him about this action
    drupal_set_title(t('You are on search page. Searching results for "!str" query.', array('!str' => $str)));

    $results = array();
    // get node types
    if($types = node_type_get_types()){
      // array of modules which provide text fields to search nodes
      $allowed_field_modules = array('text');
      // call helper function to get node's fields with text
      $search_fields = get_fields_to_search($types, $allowed_field_modules);
      // create query to search nodes in title
      $query = db_select('node', 'n')
        ->fields('n', array('nid'))
        ->condition('n.title', '%' . db_like($str) . '%', 'LIKE');
      $query->groupBy('n.nid');

      // create a subqueries for each field table to search nodes
      foreach ($search_fields as $k=>$search) {
        $_query = db_select($search['db_table'], $k);
        $_query->addField($k, 'entity_id', 'nid');
        $_query->condition('entity_type', 'node');
        $_query->condition('bundle', array_keys($types));
        $_query->condition($search['db_field'], '%' . db_like($str) . '%', 'LIKE');
        $_query->groupBy($k . '.' . $search['db_field']);

        $query->union($_query, 'UNION');
      }

      $query = $query->execute();
      $nids = $query->fetchCol();
      // check for results
      if(!$nids){
        return t('Nothing found with current query. Please try another one.');
      }
      // select nodes
      $nodes = db_select('node', 'n')->fields('n', array('nid', 'title'))->condition('nid', $nids)->execute();
      foreach($nodes as $node){
        // create links to nodes
        $results[] = l($node->title, 'node/'.$node->nid);
      }
      if($results){
        // create ul list of links to nodes
        $result = array(
          '#title' => 'List of found nodes:',
          '#theme' => 'item_list',
          '#items' => $results,
          '#type' => 'ul',
        );

        // return rendered html result
        return render($result);
      }
    } else {
      // if we have no any content type ask user to create it
      drupal_set_message(t('To use this module please create some content types !here.', array('!here' => l('here', 'admin/structure/types/add'))), 'error');
      return '';
    }
  }

  // if we have no result return this message
  return t('Please, enter some symbols to search nodes into browser address bar. (Example: !base_url/mysearch/wordstosearch)', array('!base_url' => $base_url));
}

/**
 * helper function get node fields
 */
function get_fields_to_search($types, $allowed_field_modules){
  $search_fields = array();

  foreach ($types as $type => $sett) {
    if($fields = field_info_instances('node', $sett->type)){
      foreach ($fields as $field) {
        if(in_array($field['widget']['module'], $allowed_field_modules)){
          // create array with table name and field name of node fields
          $search_fields[$field['field_name']] = array(
            'db_table' => 'field_data_' . $field['field_name'],
            'db_field' => $field['field_name'].'_value',
          );
        }
      }
    }
  }

  return $search_fields;
}
